/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.bitbucket.cryptor;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author Samba Kolusu
 * @email saasira@live.com
 * @email saasira@gmail.com
 * 
 */
public class Cryptor {
    
    public static byte[] digest(String algorithm,byte[] input, byte[] salt,int iterations) throws NoSuchAlgorithmException {
        byte[] hash=null;
        MessageDigest messageDigest;
        messageDigest = MessageDigest.getInstance(algorithm);
        for(int counter=0;counter<iterations;counter++){
            messageDigest.update(input);
            if(salt!=null) {
                messageDigest.update(salt);
            }
            hash = messageDigest.digest();
        }
        return hash;
    }
    
    public static SecretKey generateKey(char[] password, byte[] salt, int iterations) throws GeneralSecurityException {
        SecretKey secretKey;
        
        ByteBuffer bb = StandardCharsets.UTF_8.encode(CharBuffer.wrap(password));
        byte[] bytes=new byte[bb.remaining()];
        bb.get(bytes);
        
        byte[] hash=digest(Constant.SHA_256_ALGORITHM, bytes, salt, iterations); 
        
        CharBuffer cb=ByteBuffer.wrap(hash).asCharBuffer();
        char[] characters=new char[cb.remaining()];
        cb.get(characters);
        
        SecretKeyFactory kf = SecretKeyFactory.getInstance(Constant.PBE_ALGORITHM);
        PBEKeySpec keySpec = new PBEKeySpec(characters, salt, iterations,Constant.PBE_KEY_SIZE);//256 bits key
        SecretKey tempKey = kf.generateSecret(keySpec);
        secretKey = new SecretKeySpec(tempKey.getEncoded(), Constant.AES_ALGORITHM);
            
        return secretKey;
    }
    
    public static byte[] encrypt(byte[] data, char[] password) throws GeneralSecurityException {
        
        byte[] salt=new byte[20];
        int iterations= (int) (Math.random() * 100) + 10;// Short.MAX_VALUE* 100 iterations minimum  garanteed
        
        SecureRandom random = new SecureRandom();
        random.nextBytes(salt);
        
        SecretKey key=generateKey(password, salt, iterations);
        
        
        Cipher cipher = Cipher.getInstance(Constant.AES_TRANSFORMATION);
        //PBEParameterSpec parameters = new PBEParameterSpec(salt, iterations);
        byte[] iv=new byte[cipher.getBlockSize()];
        random.nextBytes(iv);
        IvParameterSpec ivspec=new IvParameterSpec(iv);
        cipher.init(Cipher.ENCRYPT_MODE, key, ivspec);
        
        byte[] encrypted=cipher.doFinal(data);
        byte[] iterationBytes=Bytes.toBytes(iterations);
        byte[] encryptedWithAlgorithmParameters=new byte[encrypted.length+salt.length+iterationBytes.length+iv.length];
        
        System.arraycopy(salt, 0, encryptedWithAlgorithmParameters, 0, salt.length);
        System.arraycopy(iterationBytes, 0, encryptedWithAlgorithmParameters, salt.length, iterationBytes.length);
        System.arraycopy(iv,0,encryptedWithAlgorithmParameters,salt.length+iterationBytes.length,iv.length);
        System.arraycopy(encrypted, 0, encryptedWithAlgorithmParameters, salt.length+iterationBytes.length+iv.length,encrypted.length);
        
        return encryptedWithAlgorithmParameters;
    }
    
    public static byte[] decrypt(byte[] gibberish, char[] password) throws GeneralSecurityException {
        byte[] data=null;
        
        Cipher cipher = Cipher.getInstance(Constant.AES_TRANSFORMATION);
        
        byte[] salt=new byte[20];
        byte[] iterationBytes=new byte[4];
        byte[] iv=new byte[cipher.getBlockSize()];
        byte[] encrypted=new byte[gibberish.length-salt.length-iterationBytes.length-iv.length];
        
        System.arraycopy(gibberish, 0, salt, 0, salt.length);
        System.arraycopy(gibberish, salt.length, iterationBytes, 0, iterationBytes.length);
        System.arraycopy(gibberish, salt.length+iterationBytes.length, iv, 0, iv.length);
        System.arraycopy(gibberish, salt.length+iterationBytes.length+iv.length, encrypted, 0, encrypted.length);
        
        int iterations=Bytes.toInt(iterationBytes);
        
        SecretKey key=generateKey(password, salt, iterations);
        
        IvParameterSpec ivspec=new IvParameterSpec(iv);
        cipher.init(Cipher.DECRYPT_MODE, key, ivspec);
        
        data=cipher.doFinal(encrypted);
        
        return data;
    }
    
    public static String checksum(java.io.InputStream is,String algorithm) throws NoSuchAlgorithmException, IOException{
        if(is==null || algorithm==null)
            return null;

        int bufferSize = 8192;
        String checksum=null;
        try (java.io.BufferedInputStream bis = new java.io.BufferedInputStream(is)) {
            int bytesRead = 0;
            byte[] buffer = new byte[bufferSize];
            byte[] hash = null;
            long startTime = System.nanoTime();
            MessageDigest digest = MessageDigest.getInstance(algorithm);

            //Keep reading from the file while there is any content
            //when the end of the stream has been reached, -1 is returned
            while ((bytesRead = bis.read(buffer)) != -1) {
                digest.update(buffer,0,bytesRead);
            }
            hash = digest.digest();
            checksum=String.valueOf(Hex.bytes2hex(hash));
            long endTime = System.nanoTime();
            //System.out.println("time taken to calculate the checksum of the contents in the input stream : "+((endTime - startTime)/(1000*1000)) +" milli seconds");
            return checksum;
        }
    }
    
}
