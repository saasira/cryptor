/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.bitbucket.cryptor;

/**
 *
 * @author Samba Kolusu
 * @email saasira@live.com
 * @email saasira@gmail.com
 * 
 */
public class Bytes {
    
    public static short toShort(byte[] bytes) {
        return  (short) ((bytes[0] & 0xFF) << 8 | (bytes[1] & 0xFF));
    }
    
    public static short toShort(byte[] bytes, int begin, int length) {
        assert length == 2;
        return  (short) ((bytes[begin] & 0xFF) << 8 | (bytes[begin+1] & 0xFF));
    }
    
    public static int toInt(byte[] bytes) {
        return bytes[0] << 24 | (bytes[1] & 0xFF) << 16 | (bytes[2] & 0xFF) << 8 | (bytes[3] & 0xFF);
    }
    
    public static int toInt(byte[] bytes, int begin, int length) {
        assert length == 4;
        return bytes[begin] << 24 | (bytes[begin+1] & 0xFF) << 16 | (bytes[begin+2] & 0xFF) << 8 | (bytes[begin+3] & 0xFF);
    }
    
    public static int toLong(byte[] bytes) {
        return bytes[0] << 54 | bytes[1] << 48 | bytes[2] << 40 | bytes[3] << 32 | bytes[4] << 24 | (bytes[5] & 0xFF) << 16 | (bytes[6] & 0xFF) << 8 | (bytes[7] & 0xFF);
    }
    
    public static long toLong(byte[] bytes, int begin, int length) {
        assert length == 8;
        return bytes[begin] << 54 | bytes[begin+1] << 48 | bytes[begin+2] << 40 | bytes[begin+3] << 32 | bytes[begin+4] << 24 | (bytes[begin+5] & 0xFF) << 16 | (bytes[begin+6] & 0xFF) << 8 | (bytes[begin+7] & 0xFF);
    }
    
    public static byte[] toBytes(short value) {                                                                     
        return new byte[] {                                                                             
            (byte) (value >> 8),                                                                        
            (byte) value
        };                                                                              
    }
    
    public static byte[] toBytes(int value) {                                                                     
        return new byte[] {                                                                             
            (byte) (value >> 24),                                                                       
            (byte) (value >> 16),                                                                       
            (byte) (value >> 8),                                                                        
            (byte) value
        };                                                                              
    }   
    
    public static byte[] toBytes(long value) {                                                                     
        return new byte[] {
            (byte) (value >> 54),                                                                       
            (byte) (value >> 48),
            (byte) (value >> 40),
            (byte) (value >> 32),
            (byte) (value >> 24),                                                                       
            (byte) (value >> 16),                                                                       
            (byte) (value >> 8),                                                                        
            (byte) value
        };                                                                              
    }

    
}

