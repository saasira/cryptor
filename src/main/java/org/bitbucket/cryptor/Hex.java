/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.bitbucket.cryptor;

/**
 *
 * @author redu
 */
public class Hex {
    
    private static final char[] HEX = "0123456789ABCDEF".toCharArray();
    
    /**
     *.
     *  convert the byte to hex format 
     * Method 1
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < raw.length; i++) {
            sb.append(Long.toString((raw[i] & 0xff) + 0x100, 0x10).substring(1));
        }
        hash=sb.toString();
    *   //convert the byte to hex format method 2
            StringBuffer hexString = new StringBuffer();
            for (int i=0;i<raw.length;i++) {
                    String hex=Integer.toHexString(0xff & raw[i]);
                    if(hex.length()==1) hexString.append('0');
                    hexString.append(hex);
            }
            hash=hexString.toString();
     * 
     *  Method Two as described above...
     *  for (int i=0;i<bytes.length;i++) {
     *       String step=Integer.toHexString(0xff & bytes[i]);
     *       if(step.length()==1)
     *           hex.append('0');
     *       hex.append(step);
     *   }
     * Method 3 : StringBuilder hex = new StringBuilder();
        
        for (int i = 0; i < bytes.length; i++) {
            hex.append(Integer.toString((bytes[i] & 0xff) + 0x100, 0x10).substring(1));
        }
        return hex.toString();
     * 
     * the implemented method is several times faster than the other methods 
     * discussed above
     * @param bytes
     * @return 
    **/
    public static String bytes2hex(byte[] bytes){
        char[] hexChars = new char[bytes.length * 2];
        for ( int index = 0; index < bytes.length; index++ ) {
            int v = bytes[index] & 0xFF;
            hexChars[index * 2] = HEX[v >>> 4];
            hexChars[index * 2 + 1] = HEX[v & 0x0F];
        }
        return new String(hexChars);
    }
    
    public static String bytes2hex(byte[] bytes, int begin, int length){
        char[] hexChars = new char[length * 2];
        for ( int index = begin; index < length; index++ ) {
            int v = bytes[index] & 0xFF;
            hexChars[index * 2] = HEX[v >>> 4];
            hexChars[index * 2 + 1] = HEX[v & 0x0F];
        }
        return new String(hexChars);
    }

    /** First Method :
     * StringBuilder buffer=new StringBuilder();
        //494c6f7665204a61766120 split into twin characters 49, 4c, 6f ...
        for( int i=0; i<hex.length()-1; i+=2 ){
            //grab the hex in pairs
            String output = hex.substring(i, (i + 2));
            //convert hex to decimal
            int decimal = Integer.parseInt(output, 16);
            //convert the decimal to character
            buffer.append((char)decimal);
        }
        String plain=buffer.toString();
        byte[] bytes=null;
        bytes = plain.getBytes(StandardCharsets.UTF_8);
        return bytes;
    * 
    * however the imeplemented method is many times faster than the other 
    * methods described above
     * @param hex
     * @return 
    **/
    public static byte[] hex2bytes(String hex){
        char[] raw=hex.toCharArray();
        byte[] bytes = new byte[raw.length / 2];
        for (int src = 0, dst = 0; dst < bytes.length; ++dst) {
            int hi = Character.digit(raw[src++], 16);
            int lo = Character.digit(raw[src++], 16);
            if ((hi < 0) || (lo < 0)) {
                throw new IllegalArgumentException();
            }
            bytes[dst] = (byte) (hi << 4 | lo);
        }
        return bytes;
    }

    
}
