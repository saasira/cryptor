/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.bitbucket.cryptor.archiver;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitOption;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.AclFileAttributeView;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.DosFileAttributeView;
import java.nio.file.attribute.FileTime;
import java.nio.file.attribute.GroupPrincipal;
import java.nio.file.attribute.PosixFileAttributeView;
import java.nio.file.attribute.PosixFilePermissions;
import java.nio.file.attribute.UserDefinedFileAttributeView;
import java.nio.file.attribute.UserPrincipal;
import java.nio.file.attribute.UserPrincipalLookupService;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.EnumSet;
import org.bitbucket.cryptor.Bytes;
import org.bitbucket.cryptor.Cryptor;

/**
 * @author Samba Kolusu
 * @email saasira@live.com
 * @email saasira@gmail.com
 **/
public class Archiver {
    
    public static void archive(final Path source, final Path target, final char[] password) throws IOException {
        
        try(ArchivingFileVisitor visitor = new ArchivingFileVisitor(source, target, password)) {
            
            Files.walkFileTree(source, EnumSet.of(FileVisitOption.FOLLOW_LINKS), Integer.MAX_VALUE,visitor);
        }
    }
    
    public static void extract(Path archive, Path directory, char[] password) throws IOException, GeneralSecurityException {
        
        Path target=null;
        BufferedOutputStream bos=null;
        
        Header header=null;
        
        byte[] buffer;
        byte[] crypted=new byte[4];//size of int
        
        int read;
        boolean filend=true;
        long fileSize=-1;
        long written=-1;//do remember that we start with -1 to avoid confusion with empty files
            
        try(BufferedInputStream bis=new BufferedInputStream(new FileInputStream(archive.toFile()))) {
            
            
            do {    //while( (read = bis.read(buffer)) > 0) {
                
                if(password!=null) {
                    read = bis.read(crypted);
                    if(read > 0) {
                        buffer=new byte[Bytes.toInt(crypted)];
                        read = bis.read(buffer);
                    } else {
                        break;//something is wrong
                    }
                } else {
                    buffer=new byte[8192];
                    read = bis.read(buffer);
                }
                
                if(password!=null) {
                    buffer=Cryptor.decrypt(buffer, password); // we decrypt the buffer at the very beginning
                    
                }
                
                if (buffer[0] == 0x00 && buffer[127] == 0x00 
                        && buffer[128] == 0x7F && buffer[255] == 0x7F 
                        && buffer[256] == 0x00 && buffer[383] == 0x00 
                        && buffer[384] == 0x7F && buffer[511] == 0x7F
                        && buffer[512] == 0x00 && buffer[639] == 0x00 
                        && buffer[640] == 0x7F && buffer[767] == 0x7F 
                        && buffer[768] == 0x00 && buffer[895] == 0x00 
                        && buffer[896] == 0x7F && buffer[1023] == 0x7F) {
                    
                    filend=Arrays.equals(buffer, Footer.getBytes());
                    if(filend) {
                        fileSize=-1;
                        written=-1;
                        if(bos!=null) {
                            bos.flush();
                            bos.close();
                        }
                        continue;// read next buffer
                    }
                }
                
                if(filend) { //as was set in the previous iteration
                    header=new Header(buffer);
                    filend=false;
                    
                    if(header.isDirectory()) {
                        target = Files.createDirectories(directory.resolve(Paths.get(header.getPath())));
                    } else if(header.isRegular()) {
                        target = Files.createFile(directory.resolve(Paths.get(header.getPath())));
                        bos=new BufferedOutputStream(new FileOutputStream(target.toFile()));
                    } else if(header.isSymlink()) {
                        target = Files.createFile(directory.resolve(Paths.get(header.getPath())));
                        target=Files.createSymbolicLink(target, Paths.get(header.getLinkPath()));
                        filend=true;
                    } else {
                        filend=true;
                        continue; //neither regular directory nor a regular file nor even a symbolic link
                    }

                    
                    
                    
                    PosixFileAttributeView posix=Files.getFileAttributeView(target,PosixFileAttributeView.class);
                    if(posix!=null) {
                        UserPrincipalLookupService lookupService = FileSystems.getDefault().getUserPrincipalLookupService();
                        UserPrincipal owner = lookupService.lookupPrincipalByName(header.getOwner());
                        posix.setOwner(owner);
                        
                        GroupPrincipal groupPrincipal = lookupService.lookupPrincipalByGroupName(header.getGroup()); 
                        posix.setGroup(groupPrincipal);
                        
                        Files.setPosixFilePermissions(target, PosixFilePermissions.fromString(header.getPosixPermissions()));
                        
                        //shouldn't we match the corresponding uname and gname before setting these ids?
                        Files.setAttribute(target, "unix:uid", (int) header.getUid(), LinkOption.NOFOLLOW_LINKS);
                        Files.setAttribute(target, "unix:gid", (int) header.getGid(), LinkOption.NOFOLLOW_LINKS);
                    }
                    
                    DosFileAttributeView dos=Files.getFileAttributeView(target, DosFileAttributeView.class, LinkOption.NOFOLLOW_LINKS);
                    if(dos!=null) {
                        dos.setReadOnly(header.isReadOnly());
                        dos.setHidden(header.isHidden());
                        dos.setArchive(header.isArchive());
                        dos.setSystem(header.isSystem());
                    }
                    
                    UserDefinedFileAttributeView userView= Files.getFileAttributeView(target, UserDefinedFileAttributeView.class, LinkOption.NOFOLLOW_LINKS);
                    if(userView!=null) {
                        
                    }
                    
                    //advised to be done last on stackoverflow, but am not sure why...
                    BasicFileAttributeView basic=Files.getFileAttributeView(target, BasicFileAttributeView.class, LinkOption.NOFOLLOW_LINKS);
                    basic.setTimes(FileTime.fromMillis(header.getCreationTime()), FileTime.fromMillis(header.getLastModifiedTime()) , FileTime.fromMillis(header.getLastAccessedTime()));
                } else {
                    if(fileSize==-1 && header!=null) {
                        fileSize=header.getSize();
                    }
                    if(target!=null && bos!=null) {
                        
                        if(fileSize - ( (written+1) + buffer.length) < 0) {     //since written starts with -1
                            bos.write(buffer,0, (int)(fileSize-written-1) );    //since written starts with -1
                        } else {
                            bos.write(buffer);
                        }
                        written+=buffer.length;
                    }
                }
            } while(read > 0);
        }
    }
    
    
    public static void main(String... clps) {
        String source=null;
        String target=null;
        String password=null;
        String option=null;
        
        boolean archive=false;
        boolean extract=false;
        
        for(String param : clps) {
            
            if(param.startsWith("-")) {
                option=param;
                switch (option) {
                    case "-a":
                        archive=true;
                        break;
                        
                    case "-e":
                        extract=true;
                        break;
                }
                //System.out.println("option = "+param);
            } else if(option!=null) {
                //System.out.println("value = "+param);
                switch (option) {
                    case "-s":
                        source=param;
                        break;
                        
                    case "-t"://text
                        target=param;
                        break;
                        
                    case "-p"://file
                        password=param;
                        break;
                }
                option=null;
            }

        }
        
        if(source==null || source.isEmpty() || target==null || target.isEmpty()) {
            throw new IllegalArgumentException("you must pass the mode of operation (-a for archive and -e for extract) source file/folder to encrypt/decrypt (with -s) and target folder to keep encrypted archive (with -t)");
        }
        
        
        try {
            if(archive) {
                Archiver.archive(Paths.get(source), Paths.get(target), password!=null?password.toCharArray():null);
            } else if(extract) {
                Archiver.extract(Paths.get(source), Paths.get(target), password!=null?password.toCharArray():null);
            }
        } catch (IOException | GeneralSecurityException ex) {
            ex.printStackTrace(System.out);
        } catch(Exception ex) {
            ex.printStackTrace(System.out);
        }
    }
}
