/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.bitbucket.cryptor.archiver;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import org.bitbucket.cryptor.Bytes;
import org.bitbucket.cryptor.Cryptor;

/**
 *
 * @author Samba Kolusu
 * @email saasira@live.com
 * @email saasira@gmail.com
 * 
 */
public class Filer {
    
    public static void encrypt(Path source, Path target, char[] password) throws IOException, GeneralSecurityException {
        BufferedInputStream bis=null;
        BufferedOutputStream bos=null;
        File sourceFile=source.toFile();
        File targetFile=target.toFile();
        
        try {
            bis=new BufferedInputStream(new FileInputStream(sourceFile));
            bos=new BufferedOutputStream(new FileOutputStream(targetFile));
            
            
            byte[] buffer=new byte[8192]; // 8 kb buffer
            byte[] encrypted=null;
            
            encrypted=Cryptor.encrypt(Bytes.toBytes((long)sourceFile.length()), password);
            bos.write(Bytes.toBytes(encrypted.length));
            bos.write(encrypted);
            
            int read=-1;
            while( (read=bis.read(buffer)) > 0 ) {
                if(read<8192) {
                    Arrays.fill(buffer, read,buffer.length, (byte)0x00);
                }
                
                encrypted=Cryptor.encrypt(buffer, password);
                bos.write(Bytes.toBytes(encrypted.length));
                bos.write(encrypted);
            }
        } finally {
            if(bis!=null) {
                bis.close();
            }
            if(bos!=null) {
                bos.close();
            }
        }
        
    }
    
    public static void decrypt(Path source, Path target, char[] password) throws IOException, GeneralSecurityException {
        BufferedInputStream bis=null;
        BufferedOutputStream bos=null;
        
        File sourceFile=source.toFile();
        File targetFile=target.toFile();
        
        try {
            bis=new BufferedInputStream(new FileInputStream(sourceFile));
            bos=new BufferedOutputStream(new FileOutputStream(targetFile));
            
            long fileSize=-1;
            long written=-1;
        
            byte[] buffer=null;
            byte[] crypted=new byte[4];
            
            int read=-1;
            
            read = bis.read(crypted);
            if(read > 0) {
                buffer=new byte[Bytes.toInt(crypted)];
                read = bis.read(buffer);
                buffer=Cryptor.decrypt(buffer, password);
                fileSize=Bytes.toLong(buffer);
            }
            
            if(read<0 || fileSize<0) {
                throw new IllegalStateException("Illegal File Size");//throw exception, this should never happen
            }
                
            do {
                
                read = bis.read(crypted);
                if(read > 0) {
                    buffer=new byte[Bytes.toInt(crypted)];
                    read = bis.read(buffer);
                } else {
                    break;//something is wrong, shold we throw exeption?
                }
                
                buffer=Cryptor.decrypt(buffer, password);
                
                if(fileSize - ( (written+1) + buffer.length) < 0) {     //since written starts with -1
                    bos.write(buffer,0, (int)(fileSize-written-1) );    //since written starts with -1
                } else {
                    bos.write(buffer);
                }
                written+=buffer.length;
                
            } while( read > 0 );
            
        } finally {
            if(bis!=null) {
                bis.close();
            }
            if(bos!=null) {
                bos.close();
            }
        }
        
    }
    
    public static void main(String... clps) {
        
        String source=null;
        String target=null;
        String password=null;
        String option=null;
        
        boolean encrypt=false;
        boolean decrypt=false;
        
        for(String param : clps) {
            
            if(param.startsWith("-")) {
                option=param;
                switch (option) {
                    case "-e":
                        encrypt=true;
                        break;
                        
                    case "-d":
                        decrypt=true;
                        break;
                }
            } else if(option!=null) {
                switch (option) {
                    case "-s":
                        source=param;
                        break;
                        
                    case "-t"://text
                        target=param;
                        break;
                        
                    case "-p"://file
                        password=param;
                        break;
                }
                option=null;
            }

        }
        
        if(source==null || source.isEmpty() || target==null || target.isEmpty()) {
            throw new IllegalArgumentException("you must pass the mode of operation (-e for encrypt and -d for decrypt) source file/folder to encrypt/decrypt (with -s) and target folder to keep encrypted archive (with -t)");
        }
        
        
        try {
            if(encrypt) {
                encrypt(Paths.get(source), Paths.get(target), password!=null?password.toCharArray():null);
            } else if(decrypt) {
                decrypt(Paths.get(source), Paths.get(target), password!=null?password.toCharArray():null);
            }
        } catch (IOException  | GeneralSecurityException ex) {
            ex.printStackTrace(System.out);
        } catch(Throwable ex) {
            ex.printStackTrace(System.out);
        }
        
    }
}
