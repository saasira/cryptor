/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.cryptor.archiver;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.AclFileAttributeView;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.DosFileAttributeView;
import java.nio.file.attribute.FileOwnerAttributeView;
import java.nio.file.attribute.PosixFileAttributeView;
import java.nio.file.attribute.UserDefinedFileAttributeView;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import org.bitbucket.cryptor.Bytes;
import org.bitbucket.cryptor.Cryptor;

/**
 *
 * @author redu
 */
public class ArchivingFileVisitor implements FileVisitor<Path>, AutoCloseable {

    private final char[] password;
    private final Path source;
    private final Path target;
    private Path parent;
    
    private final BufferedOutputStream bos;
    
    private final byte[] buffer = new byte[8192];//data buffer
    private byte[] encrypted = null;
    
    public ArchivingFileVisitor(final Path source, final Path target, final char[] password) throws IOException {
        this.source=source;
        this.target = Files.createFile(target); // ensure that this file does not exist before, and create new 
        this.password = password;
        this.parent=this.source.getParent();
        
        //set the acl from source directory to the target archive
        AclFileAttributeView acl = Files.getFileAttributeView(this.source, AclFileAttributeView.class);
        if (acl != null) {
            Files.getFileAttributeView(this.target, AclFileAttributeView.class).setAcl(acl.getAcl());
        }
                
        this.bos =new BufferedOutputStream(new FileOutputStream(this.target.toFile()));
    }
    
    
    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes sourceBasic)
            throws IOException {
        PosixFileAttributeView posix = Files.getFileAttributeView(dir, PosixFileAttributeView.class);
        DosFileAttributeView dos = Files.getFileAttributeView(dir, DosFileAttributeView.class);
        AclFileAttributeView acl = Files.getFileAttributeView(dir, AclFileAttributeView.class);
        FileOwnerAttributeView owner = Files.getFileAttributeView(dir, FileOwnerAttributeView.class);
        BasicFileAttributeView basic = Files.getFileAttributeView(dir, BasicFileAttributeView.class);
        UserDefinedFileAttributeView user = Files.getFileAttributeView(dir, UserDefinedFileAttributeView.class);

        Header header = new Header(parent, dir, basic, acl, owner, user, dos, posix);

        if (password != null) {
            try {
                encrypted = Cryptor.encrypt(header.getBytes(), password);
                bos.write(Bytes.toBytes(encrypted.length));
                bos.write(encrypted);

                //directory does not contain any content apart from child files
                encrypted = Cryptor.encrypt(Footer.getBytes(), password);
                bos.write(Bytes.toBytes(encrypted.length));
                bos.write(encrypted);
            } catch (GeneralSecurityException gse) {
                throw new RuntimeException(gse);
            }
        } else {
            bos.write(header.getBytes());
            //directory does not contain any content apart from child files
            bos.write(Footer.getBytes());
        }
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {

        PosixFileAttributeView posix = Files.getFileAttributeView(file, PosixFileAttributeView.class);
        DosFileAttributeView dos = Files.getFileAttributeView(file, DosFileAttributeView.class);
        AclFileAttributeView acl = Files.getFileAttributeView(file, AclFileAttributeView.class);
        FileOwnerAttributeView owner = Files.getFileAttributeView(file, FileOwnerAttributeView.class);
        BasicFileAttributeView basic = Files.getFileAttributeView(file, BasicFileAttributeView.class);
        UserDefinedFileAttributeView user = Files.getFileAttributeView(file, UserDefinedFileAttributeView.class);

        Header header = new Header(parent, file, basic, acl, owner, user, dos, posix);

        try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file.toFile()))) {
            if (password != null) {
                try {
                    encrypted = Cryptor.encrypt(header.getBytes(), password);
                    bos.write(Bytes.toBytes(encrypted.length));
                    bos.write(encrypted);
                } catch (GeneralSecurityException gse) {
                    throw new RuntimeException(gse);
                }
            } else {
                bos.write(header.getBytes());
            }
            int read;
            while ((read = bis.read(buffer)) > 0) {
                if (read < buffer.length) {
                    Arrays.fill(buffer, read, buffer.length, (byte) 0x00);
                    //buffer=Arrays.copyOfRange(buffer, 0, read+1);
                }

                if (password != null) {
                    try {
                        encrypted = Cryptor.encrypt(buffer, password);
                        bos.write(Bytes.toBytes(encrypted.length));
                        bos.write(encrypted);
                    } catch (GeneralSecurityException gse) {
                        throw new RuntimeException(gse);
                    }
                } else {
                    bos.write(buffer);
                }
            }

            if (password != null) {
                try {
                    encrypted = Cryptor.encrypt(Footer.getBytes(), password);
                    bos.write(Bytes.toBytes(encrypted.length));
                    bos.write(encrypted);
                } catch (GeneralSecurityException gse) {
                    throw new RuntimeException(gse);
                }
            } else {
                bos.write(Footer.getBytes());
            }
        }

        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Path file, IOException e) throws IOException {
        throw e;
    }

    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException e) throws IOException {
        if (e != null) {
            throw e;
        }
        return FileVisitResult.CONTINUE;
    }

    @Override
    public void close() throws IOException {
        bos.close();
    }
}
