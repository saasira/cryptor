/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.cryptor.archiver;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.AclFileAttributeView;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.DosFileAttributeView;
import java.nio.file.attribute.FileOwnerAttributeView;
import java.nio.file.attribute.PosixFileAttributeView;
import java.nio.file.attribute.PosixFileAttributes;
import java.nio.file.attribute.PosixFilePermissions;
import java.nio.file.attribute.UserDefinedFileAttributeView;
import java.security.NoSuchAlgorithmException;
import org.bitbucket.cryptor.Bytes;
import org.bitbucket.cryptor.Constant;
import org.bitbucket.cryptor.Cryptor;
import org.bitbucket.cryptor.Hex;

/**
 * @author Samba Kolusu
 * @email saasira@live.com
 * @email saasira@gmail.com
 *
 */
/**
 * one kilo byte of file meta data name : 0-255 [256] size : 256-263 [8]
 * checksum : 264-295 [32] creationTime : 296-303 [8] lastModifiedTime : 304-311
 * [8] lastAccessedTime : 312-319 [8] type : 320-327 [8]
 * 
* uid : 328-335 [8] gid : 336-343 [8] sid : 344-351 [8] //not implemented uname
 * : 352-383 [32] gname : 384-415 [32] posixPermissions : 416-427 [12]
 * windowsPermissions : 428-439 [12] //not implemented mode : 440-447 [8]
 * 
* linkName : 448-703 [256]
 * 
* deviceMajor : 704-711 [8] //not implemented deviceMinor : 712-719 [8] prefix
 * : 720-975 [256] //not implemented
 * 
* magic : 976-987 [12] version : 988-989 [2] //update not implemented
 * 
*
 */
public class Header {

    public static final byte REGULAR = 0x10;
    public static final byte DIRECTORY = 0x20;
    public static final byte SYMLINK = 0x30;
    public static final byte OTHER = 0x40;

    public static final byte READONLY = 0x11;
    public static final byte HIDDEN = 0x12;
    public static final byte ARCHIVE = 0x13;
    public static final byte SPARSE = 0x14;   //not implemented
    public static final byte SYSTEM = 0x15;

    public static final byte BLOCK = 0x41;
    public static final byte FIFO = 0x42;

    public static final String MAGIC = "cryptarchive";
    private BasicFileAttributeView basicAttrs;
    private AclFileAttributeView aclAttrs;
    private FileOwnerAttributeView ownerAttrs;
    private UserDefinedFileAttributeView userAttrs;
    private DosFileAttributeView dosAttrs;
    private PosixFileAttributeView posixAttrs;

    private final byte[] bytes;

    private String path;
    private long size;
    private String checksum;
    private long creationTime;
    private long lastModifiedTime;
    private long lastAccessedTime;

    private boolean regular;
    private boolean directory;
    private boolean symlink;
    private boolean other;

    private boolean readOnly;
    private boolean hidden;
    private boolean archive;
    private boolean sparse;
    private boolean system;

    private long uid;
    private long gid;
    private String uname;
    private String gname;
    private String owner;
    private String group;
    private String posixPermissions;

    private String linkPath;
    private String deviceMajor;
    private String deviceMinor;
    private String prefix;

    private int mode;

    private String magic;
    private short version;

    public Header(Path parent, Path source, BasicFileAttributeView basicAttrsView, AclFileAttributeView aclAttrsView, FileOwnerAttributeView ownerAttrsView, UserDefinedFileAttributeView userAttrsView, DosFileAttributeView dosAttrsView, PosixFileAttributeView posixAttrsView) throws IOException {

        this.basicAttrs = basicAttrsView;
        this.aclAttrs = aclAttrsView;
        this.ownerAttrs = ownerAttrsView;
        this.userAttrs = userAttrsView;
        this.dosAttrs = dosAttrsView;
        this.posixAttrs = posixAttrsView;
        this.bytes = new byte[1024];//create new

        byte[] temp;

        if (basicAttrsView != null) {
            BasicFileAttributes basicAttributes = basicAttrsView.readAttributes();

            temp = parent.relativize(source).toString().getBytes(StandardCharsets.UTF_8);
            System.arraycopy(temp, 0, this.bytes, 0, Math.min(temp.length, 255));//name
            System.arraycopy(Bytes.toBytes(basicAttributes.size()), 0, this.bytes, 256, 8);//size

            if (basicAttributes.isRegularFile()) {
                try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream(source.toFile()))) {
                    checksum = Cryptor.checksum(bis, Constant.SHA1_ALGORITHM);
                    temp = checksum.getBytes(StandardCharsets.UTF_8);
                    System.arraycopy(temp, 0, this.bytes, 264, temp.length);//16 bytes for sha1 and 32 bytes for sha-256
                } catch (NoSuchAlgorithmException ex) {
                    //should never occur, since we give a known algorithm; perhaps we may swallow this exception
                }
            }

            System.arraycopy(Bytes.toBytes(basicAttributes.creationTime().toMillis()), 0, this.bytes, 296, 8);//creation time
            System.arraycopy(Bytes.toBytes(basicAttributes.lastModifiedTime().toMillis()), 0, this.bytes, 304, 8);//last modified time
            System.arraycopy(Bytes.toBytes(basicAttributes.lastAccessTime().toMillis()), 0, this.bytes, 312, 8);//last accessed time

            byte type = 0x00;
            if (basicAttributes.isDirectory()) {
                type = DIRECTORY;
                directory = true;
            } else if (basicAttributes.isRegularFile()) {
                type = REGULAR;
                regular = true;
            } else if (basicAttributes.isSymbolicLink()) {
                type = SYMLINK;
                symlink = true;
            } else if (basicAttributes.isOther()) {
                type = OTHER;
                other = true;
            }

            this.bytes[320] = type;

        }

        if (dosAttrsView != null) {
            readOnly = dosAttrsView.readAttributes().isReadOnly();   //(boolean) Files.getAttribute(source, "dos:readonly");
            hidden = dosAttrsView.readAttributes().isHidden();   //(boolean) Files.getAttribute(source, "dos:hidden");
            archive = dosAttrsView.readAttributes().isArchive();    //(boolean)  Files.getAttribute(source, "dos:archive");
            system = dosAttrsView.readAttributes().isSystem();  //(boolean) Files.getAttribute(source, "dos:system");
        }

        if (regular) {
            if (readOnly) {
                this.bytes[321] = READONLY;
            }
            if (hidden) {
                this.bytes[322] = HIDDEN;
            }
            if (archive) {
                this.bytes[323] = ARCHIVE;
            }

            if (system) {
                this.bytes[325] = SYSTEM;
            }
        }

        if (posixAttrsView != null) {
            PosixFileAttributes posixAttributes = posixAttrsView.readAttributes();

            mode = (Integer) Files.getAttribute(source, "unix:mode");

            /*
             //better not use these attributes for they make the archive device & file system dependent
             long ino = (Long)Files.getAttribute(source, "unix:ino");
             long dev = (Long)Files.getAttribute(source, "unix:dev");
             long rdev = (Long)Files.getAttribute(source, "unix:rdev");
             int nlink = (Integer)Files.getAttribute(source, "unix:nlink");
             */
            uid = (Integer) Files.getAttribute(source, "unix:uid");
            gid = (Integer) Files.getAttribute(source, "unix:gid");

            System.arraycopy(Bytes.toBytes(uid), 0, this.bytes, 328, 8);//uid
            System.arraycopy(Bytes.toBytes(gid), 0, this.bytes, 336, 8);//uid

            owner = posixAttributes.owner().getName();
            group = posixAttributes.group().getName();
            temp = owner.getBytes(StandardCharsets.UTF_8);
            System.arraycopy(temp, 0, this.bytes, 352, temp.length);//32 bytes 
            temp = group.getBytes(StandardCharsets.UTF_8);
            System.arraycopy(temp, 0, this.bytes, 384, temp.length);//32 bytes 
            temp = PosixFilePermissions.toString(posixAttributes.permissions()).getBytes(StandardCharsets.UTF_8);
            System.arraycopy(temp, 0, this.bytes, 416, temp.length);//32 bytes 

            System.arraycopy(Bytes.toBytes(mode), 0, this.bytes, 440, 4);//mode
        }

        if (symlink) {
            Path link = Files.readSymbolicLink(source);
            temp = link.toString().getBytes(StandardCharsets.UTF_8);
            System.arraycopy(temp, 0, this.bytes, 448, Math.min(temp.length, 256));
        }

        System.arraycopy(MAGIC.getBytes(StandardCharsets.UTF_8), 0, this.bytes, 976, 12);
        System.arraycopy(Bytes.toBytes((short) 1), 0, this.bytes, 988, 2);//uid

    }

    public Header(byte[] header) {

        if (header.length != 1024) {
            throw new IllegalArgumentException("Illegal Header block");
        }

        this.bytes = header;

            //System.arraycopy(this.bytes, 0, name, 0, name.length);//name.length=256
        //System.arraycopy(this.bytes, 256, size, 0, size.length);
        //System.arraycopy(this.bytes, 268, checksum, 0, checksum.length);
        path = new String(this.bytes, 0, 256, StandardCharsets.UTF_8).replace((char) 0x00, ' ').trim();
        size = Bytes.toLong(this.bytes, 256, 8);//8 the to index for Arrays.copyOfRange is exclusive, and hence we need to give one byte extra
        checksum = Hex.bytes2hex(this.bytes, 264, 32);//32
        creationTime = Bytes.toLong(this.bytes, 296, 8);//8
        lastModifiedTime = Bytes.toLong(this.bytes, 304, 8);//8
        lastAccessedTime = Bytes.toLong(this.bytes, 312, 8);//8

        regular = false;
        directory = false;
        symlink = false;
        other = false;

        if (REGULAR == this.bytes[320]) {
            regular = true;
        } else if (DIRECTORY == this.bytes[320]) {
            directory = true;
        } else if (SYMLINK == this.bytes[320]) {
            symlink = true;
        } else if (OTHER == this.bytes[320]) {
            other = true;
        }

        readOnly = false;
        hidden = false;
        archive = false;
        sparse = false;
        system = false;

        if (regular) {
            if (READONLY == this.bytes[321]) {
                readOnly = true;
            }
            if (HIDDEN == this.bytes[322]) {
                hidden = true;
            }
            if (ARCHIVE == this.bytes[323]) {
                archive = true;
            }
            if (SPARSE == this.bytes[324]) {
                sparse = true;
            }
            if (SYSTEM == this.bytes[325]) {
                system = true;
            }
        }

        uid = Bytes.toLong(this.bytes, 328, 8);//8
        gid = Bytes.toLong(this.bytes, 336, 8);//8
        owner = new String(this.bytes, 352, 32, StandardCharsets.UTF_8).replace((char) 0x00, ' ').trim();
        group = new String(this.bytes, 384, 32, StandardCharsets.UTF_8).replace((char) 0x00, ' ').trim();
        posixPermissions = new String(this.bytes, 416, 12, StandardCharsets.UTF_8).replace((char) 0x00, ' ').trim();

        linkPath = new String(this.bytes, 448, 256, StandardCharsets.UTF_8).replace((char) 0x00, ' ').trim();
        deviceMajor = new String(this.bytes, 704, 8, StandardCharsets.UTF_8).replace((char) 0x00, ' ').trim();
        deviceMinor = new String(this.bytes, 712, 8, StandardCharsets.UTF_8).replace((char) 0x00, ' ').trim();
        prefix = new String(this.bytes, 720, 256, StandardCharsets.UTF_8).replace((char) 0x00, ' ').trim();//256

        magic = new String(this.bytes, 976, 12, StandardCharsets.UTF_8).replace((char) 0x00, ' ').trim();//12 
        version = Bytes.toShort(this.bytes, 988, 2);//2

    }

    public byte[] getBytes() {
        return bytes;
    }

    public static String getMAGIC() {
        return MAGIC;
    }

    public String getPath() {
        return path;
    }

    public long getSize() {
        return size;
    }

    public String getChecksum() {
        return checksum;
    }

    public long getCreationTime() {
        return creationTime;
    }

    public long getLastModifiedTime() {
        return lastModifiedTime;
    }

    public long getLastAccessedTime() {
        return lastAccessedTime;
    }

    public boolean isRegular() {
        return regular;
    }

    public boolean isDirectory() {
        return directory;
    }

    public boolean isSymlink() {
        return symlink;
    }

    public boolean isOther() {
        return other;
    }

    public boolean isReadOnly() {
        return readOnly;
    }

    public boolean isHidden() {
        return hidden;
    }

    public boolean isArchive() {
        return archive;
    }

    public boolean isSparse() {
        return sparse;
    }

    public boolean isSystem() {
        return system;
    }

    public long getUid() {
        return uid;
    }

    public long getGid() {
        return gid;
    }

    public String getOwner() {
        return owner;
    }

    public String getGroup() {
        return group;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getGname() {
        return gname;
    }

    public void setGname(String gname) {
        this.gname = gname;
    }

    public String getPosixPermissions() {
        return posixPermissions;
    }

    public String getLinkPath() {
        return linkPath;
    }

    public String getDeviceMajor() {
        return deviceMajor;
    }

    public String getDeviceMinor() {
        return deviceMinor;
    }

    public String getPrefix() {
        return prefix;
    }

    public int getMode() {
        return mode;
    }

    public String getMagic() {
        return magic;
    }

    public short getVersion() {
        return version;
    }

}
