/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.bitbucket.cryptor.archiver;

import java.util.Arrays;

/**
 *
 * @author redu
 */
public class Footer {
    
    private static final byte[] bytes = new byte[1024];
        
        static {
            Arrays.fill(bytes, 0, 128, (byte)0x00); // NUL char
            Arrays.fill(bytes, 128, 256, (byte)0x7F);//DEL char
            Arrays.fill(bytes, 256, 384, (byte)0x00); // NUL char
            Arrays.fill(bytes, 384, 512, (byte)0x7F);//DEL char
            Arrays.fill(bytes, 512, 640, (byte)0x00); // NUL char
            Arrays.fill(bytes, 640, 768, (byte)0x7F);//DEL char
            Arrays.fill(bytes, 768, 896, (byte)0x00); // NUL char
            Arrays.fill(bytes, 896, 1024, (byte)0x7F);//DEL char
        }

        public static byte[] getBytes() {
            return bytes;
        }
    
    
}
