/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.cryptor.archiver;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
import javax.crypto.BadPaddingException;

/**
 *
 * @author redu
 */
public class Zipper {

    public static void archive(Path sourceDirectory, Path encryptedArchive, char[] password) throws IOException, GeneralSecurityException {
        //String zipFileName=targetFile.getParent().toString()+File.separator+sourceDirectory.getFileName()+".zip";
        //String encryptedFileName=targetFile.toString()+File.separator+sourceDirectory.getFileName()+".enc";
        
        Path zipFile=encryptedArchive.getParent().resolve(sourceDirectory.getFileName()+".zip");
        
        //zip(sourceDirectory, Paths.get(zipFileName));
        //Filer.encrypt(Paths.get(zipFileName) , Paths.get(encryptedFileName), password);
        //Paths.get(zipFileName).toFile().delete();//delete the intermediate zip file
        zip(sourceDirectory,zipFile);
        Filer.encrypt(zipFile , encryptedArchive, password);
        zipFile.toFile().delete();
        
    }

    public static void extract(Path encryptedArchive, Path targetDirectory, char[] password) throws IOException, GeneralSecurityException {
        String encryptedFileName=encryptedArchive.getFileName().toString();
        String zipFileName=targetDirectory.toString()+File.separator+encryptedFileName.substring(0, encryptedFileName.lastIndexOf("."))+".zip";
        String extractedDirectory=targetDirectory.toString()+File.separator+encryptedFileName.substring(0, encryptedFileName.lastIndexOf("."));
        
        Filer.decrypt(encryptedArchive, Paths.get(zipFileName), password);
        unzip(Paths.get(zipFileName), Paths.get(extractedDirectory));
        Paths.get(zipFileName).toFile().delete();
    }
    
    
    private static void getAllFiles(File dir, List<File> fileList) throws IOException {
        File[] files = dir.listFiles();
        for (File file : files) {
            fileList.add(file);
            
            if (file.isDirectory()) {
                getAllFiles(file, fileList);
            }
        }
    }

    public static void zip(Path directoryToZip, Path zipFile) throws IOException {
        FileOutputStream fos = null;
        ZipOutputStream zos = null;
        
        List<File> fileList=new ArrayList<>();
        getAllFiles(directoryToZip.toFile(),fileList);
        
        try {
            fos = new FileOutputStream(zipFile.toFile());
            zos = new ZipOutputStream(fos);

            for (File file : fileList) {
                if (!file.isDirectory()) { // we only zip files, not directories
                    addToZip(directoryToZip.toFile(), file, zos);
                }
            }
        } finally {
            if (zos != null) {
                zos.close();
            }
            if (fos != null) { // it is important to close zip output stream before we close file output stream
                fos.close();
            }
            
        }
    }

    public static void addToZip(File directoryToZip, File file, ZipOutputStream zos) throws FileNotFoundException,
            IOException {

         // we want the zipEntry's path to be a relative path that is relative
        // to the directory being zipped, so chop off the rest of the path
        try (FileInputStream fis = new FileInputStream(file)) {
             // we want the zipEntry's path to be a relative path that is relative
            // to the directory being zipped, so chop off the rest of the path
            //String zipFilePath = file.getCanonicalPath().substring(directoryToZip.getCanonicalPath().length() + 1,
            //        file.getCanonicalPath().length());
            String zipFilePath = directoryToZip.toPath().getParent().relativize(file.toPath()).toString();
            //System.out.println("Writing '" + zipFilePath + "' to zip file");
            ZipEntry zipEntry = new ZipEntry(zipFilePath);
            zos.putNextEntry(zipEntry);

            byte[] bytes = new byte[1024];
            int length;
            while ((length = fis.read(bytes)) >= 0) {
                zos.write(bytes, 0, length);
            }

            zos.closeEntry();
        }
    }

    /**
     * Unzip it
     *
     * @param zipFile input zip file
     * @param outputFolder
     * @throws java.io.IOException
     */
    public static void unzip(Path zipFile, Path outputFolder) throws IOException {

        byte[] buffer = new byte[1024];

        if(!outputFolder.toFile().exists()) {
            outputFolder.toFile().mkdir();
        }

        //get the zipped file list entry
        //get the zip file content
        try (ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile.toFile()))) {
            //get the zipped file list entry
            ZipEntry ze = zis.getNextEntry();

            while (ze != null) {

                String fileName = ze.getName();
                //Paths.get(outputFolder).relativize(Paths.get(fileName));
                
                File newFile = new File(outputFolder.toAbsolutePath() + File.separator + fileName);

                //System.out.println("file unzip : " + newFile.getAbsoluteFile());

                //create all non exists folders
                //else you will hit FileNotFoundException for compressed folder
                new File(newFile.getParent()).mkdirs();

                try (FileOutputStream fos = new FileOutputStream(newFile)) {
                    int len;
                    while ((len = zis.read(buffer)) > 0) {
                        fos.write(buffer, 0, len);
                    }
                }
                ze = zis.getNextEntry();
            }

            zis.closeEntry();
        }

        //System.out.println("Done");

    }

    public static void main(String... clps) throws IOException {
        String source=null;
        String target=null;
        String password=null;
        String option=null;
        
        boolean archive=false;
        boolean extract=false;
        
        for(String param : clps) {
            
            if(param.startsWith("-")) {
                option=param;
                switch (option) {
                    case "-a":
                        archive=true;
                        break;
                        
                    case "-e":
                        extract=true;
                        break;
                }
                //System.out.println("option = "+param);
            } else if(option!=null) {
                //System.out.println("value = "+param);
                switch (option) {
                    case "-s":
                        source=param;
                        break;
                        
                    case "-t"://text
                        target=param;
                        break;
                        
                    case "-p"://file
                        password=param;
                        break;
                }
                option=null;
            }

        }
        
        if(source==null || source.isEmpty() || target==null || target.isEmpty()) {
            throw new IllegalArgumentException("you must pass the mode of operation (-a for archive and -e for extract) source file/folder to encrypt/decrypt (with -s) and target folder to keep encrypted archive (with -t)");
        }
        
        
        try {
            if(archive) {
                Zipper.archive(Paths.get(source), Paths.get(target), password!=null?password.toCharArray():null);
            } else if(extract) {
                Zipper.extract(Paths.get(source), Paths.get(target), password!=null?password.toCharArray():null);
            }
        } catch (IOException ex) {
            ex.printStackTrace(System.out);
        } catch(GeneralSecurityException gse) {
            if(gse instanceof BadPaddingException) {
                System.out.println("either the encrypted archive is corrupted or you supplied a wrong password");
            }
        } catch(Exception ex) {
            ex.printStackTrace(System.out);
        }
    }
}
