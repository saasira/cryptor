/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.bitbucket.cryptor;

/**
 *
 * @author redu
 */
public class Constant {
    
    public static final String AES_ALGORITHM="AES";
    public static final String AES_TRANSFORMATION="AES/CBC/PKCS5Padding";
    public static final int AES_KEY_SIZE=256;
    
    public static final String SHA1_ALGORITHM="SHA1";
    public static final String SHA_256_ALGORITHM="SHA-256";
    
    public static final String PBE_ALGORITHM="PBKDF2WithHmacSHA1";//PBEWithMD5AndTripleDES
    public static final int PBE_KEY_SIZE=256;
    
}
